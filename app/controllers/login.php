<?php namespace controllers;
use core\view as View;

/*
 * Login controller
 */
class Login extends \core\controller{

	/**
	 * call the parent construct
	 */
	public function __construct(){
		parent::__construct();

		$this->language->load('login');
	}

	/**
	 * define page title and load template files
	 */
	public function login(){

		$data['title'] = 'Login';
		$data['welcome_message'] = $this->language->get('welcome_message');

		if(isset($_SESSION['user'])){
			$data['error_message'] = "Vous etes deja connecte en tant que " .$_SESSION['user']->name;
			View::rendertemplate('header', $data);
			View::rendertemplate('footer', $data);
		} else {
			if(isset($_POST['email']) && isset($_POST['password'])){
				$curl = curl_init();
				$apikey = "544533154f8259331100006b";
				$email = htmlentities($_POST['email']);
				$password = htmlentities($_POST['password']);
	
				$params = "apikey=$apikey&email=$email&password=$password";
 	
				$curl = curl_init();
 	
				curl_setopt($curl, CURLOPT_URL, "http://code.ducking-games.io/blindtest/auth/login");
				curl_setopt($curl, CURLOPT_POSTFIELDS, $params); 
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
				$return = json_decode(utf8_encode(curl_exec($curl))); 
	
				curl_close($curl);
 			
 				if(isset($return->name)){
 					$_SESSION['user'] = $return;
 					$data['good_message'] = "Vous etes maintenant connecte en tant que " . $_SESSION['user']->name;
 					View::render('login/login', $data);
					View::rendertemplate('footer', $data);
 				} else {
 					$data['error_message'] = "Erreur lors de la connexion";
 					View::rendertemplate('header', $data);
					View::render('login/login', $data);
					View::rendertemplate('footer', $data);
 				}
	
			} else {
				View::rendertemplate('header', $data);
				View::render('login/login', $data);
				View::rendertemplate('footer', $data);
			}
		}
		

		
	}

	public function logout(){

		$data['title'] = 'Logout';
		$data['welcome_message'] = $this->language->get('welcome_message');

		$apikey = "544533154f8259331100006b";
		
		$params = "apikey=$apikey&email=$email&password=$password";
 
		$curl = curl_init();

 		curl_setopt($curl, CURLOPT_URL, "http://code.ducking-games.io/blindtest/auth/logout");
		
		curl_setopt($curl, CURLOPT_POSTFIELDS, $params); 
		
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		
		$return = json_decode(utf8_encode(curl_exec($curl))); 
		
		curl_close($curl);

		$_SESSION['user'] = null;
		
		$data['good_message'] = "Vous etes maintenant deconnecte";
		View::rendertemplate('header', $data);
		View::rendertemplate('footer', $data);
		
	}

	public function profile(){

		$data['title'] = 'Profil';
		$data['welcome_message'] = $this->language->get('welcome_message');

		if(isset($_SESSION['user'])){

			$data['user'] = $_SESSION['user'];
			
			View::rendertemplate('header', $data);
			View::render('login/profile', $data);
			View::rendertemplate('footer', $data);
		} else {
			$data['error_message'] = "Vous n'etes pas connecte";
			View::rendertemplate('header', $data);
			View::rendertemplate('footer', $data);
		}
		
	}

}
