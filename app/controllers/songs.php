<?php namespace controllers;
use core\view as View;

/*
 * Login controller
 *
 * @author David Carr - dave@daveismyname.com - http://www.daveismyname.com
 * @version 2.1
 * @date June 27, 2014
 */
class Songs extends \core\controller{

	/**
	 * call the parent construct
	 */
	public function __construct(){
		parent::__construct();

		$this->language->load('welcome');
	}

	/**
	 * define page title and load template files
	 */
	public function songsRandom(){
		if(!isset($_SESSION['user'])){
			//ajouter message erreur
			View::rendertemplate('header');
			View::render('login', $data);
			View::rendertemplate('footer');
		}

		$curl = curl_init();
		$apikey = "544533154f8259331100006b";

		$params = "apikey=$apikey";

		$curl = curl_init();

		curl_setopt($curl, CURLOPT_URL, "http://code.ducking-games.io/blindtest/song/random");
		curl_setopt($curl, CURLOPT_POSTFIELDS, $params); //On envoie les valeurs
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		$return = curl_exec($curl); //On place le code de la page dans $sh4Code

		curl_close($curl);

		$data['songs'] =  json_decode(utf8_encode($return)) ;
		
		
		
		View::rendertemplate('header');
		View::render('song/songsrandom', $data);
		View::rendertemplate('footer');

	}

	

	public function repondreSong(){
		if(!isset($_SESSION['user'])){
			//ajouter message erreur
			View::rendertemplate('header');
			View::render('login', $data);
			View::rendertemplate('footer');
		}

		$idSong = htmlentities($_POST['id']);
		$reponse = htmlentities($_POST['reponse']);


		$curl = curl_init();
		$apikey = "544533154f8259331100006b";
		$params = "apikey=$apikey";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, "http://code.ducking-games.io/blindtest/song/".$idSong);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $params); //On envoie les valeurs
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		$return = curl_exec($curl); //On place le code de la page dans $sh4Code

		curl_close($curl);

		$song =  json_decode(utf8_encode($return)) ;

		$nbPoint=0;

		$origArt = str_replace(" ", "", strtolower($song->artist));
		$origName = str_replace(" ", "", strtolower($song->name));
		
		$userRep = str_replace(" ", "", strtolower($reponse));


		$levenArtiste = levenshtein($origArt, $userRep );
		$levenChanson = levenshtein($origName ,$userRep );

		$levenArtisteChanson = levenshtein($origArt.$origName, $userRep);
		$levenChansonArtiste = levenshtein($origName.$origArt, $userRep);


		if($levenArtisteChanson<4 || $levenChansonArtiste<4){
			$nbPoint = $nbPoint+20;
			$trouve=true;
		}else{
			if($levenArtiste<2){
				$nbPoint = $nbPoint+10;
				$trouve=true;
			}
			if($levenChanson<2){
				$nbPoint = $nbPoint+10;
				$trouve=true;
			}
		}


		if($nbPoint!=0){
			$user = $_SESSION['user'];

			$curl = curl_init();
			$apikey = "544533154f8259331100006b";
			$params = "apikey=$apikey&score=$nbPoint";
			$curl = curl_init();
			$url = "http://code.ducking-games.io/blindtest/score/$user->_id/$idSong";
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			$res = curl_exec($curl);
			
		}


		$data['trouve']=$trouve;
		$data['nbPoint']=$nbPoint;

		View::rendertemplate('header');
		View::render('song/result', $data);
		View::rendertemplate('footer');

	}

}
