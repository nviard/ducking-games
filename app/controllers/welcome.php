<?php namespace controllers;
use core\view as View;

/*
 * Welcome controller
 *
 * @author David Carr - dave@daveismyname.com - http://www.daveismyname.com
 * @version 2.1
 * @date June 27, 2014
 */
class Welcome extends \core\controller{

	/**
	 * call the parent construct
	 */
	public function __construct(){
		parent::__construct();

		$this->language->load('welcome');
	}

	/**
	 * define page title and load template files
	 */
	public function index(){

		$curl = curl_init();
			$apikey = "544533154f8259331100006b";

			$params = "apikey=$apikey";
 
			$curl = curl_init();
 
			curl_setopt($curl, CURLOPT_URL, "http://code.ducking-games.io/blindtest/users");
			curl_setopt($curl, CURLOPT_POSTFIELDS, $params); 
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			$users = curl_exec($curl); 
 	
 			$users = json_decode(utf8_encode($users)); 
 	
 			foreach ($users as  $user) {
 				$data['users'][$user->_id]['name'] = $user->name;
 				$data['users'][$user->_id]['score'] = 0;
 				curl_setopt($curl, CURLOPT_URL, "http://code.ducking-games.io/blindtest/score/" . $user->_id);
				curl_setopt($curl, CURLOPT_POSTFIELDS, $params); 
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
				$scores = curl_exec($curl);
 	
 				$scores = json_decode(utf8_encode($scores)); 
   				foreach ($scores as $score) {
  					$data['users'][$user->_id]['score'] += $score->score;
  				}

			}

			curl_close($curl);

			uasort($data['users'],
				function($a, $b){
    				return $b['score'] - $a['score'];
				}
			);

			View::rendertemplate('header', $data);
			View::render('scores/scores', $data);
			View::rendertemplate('footer', $data);
			
 
	}

}
