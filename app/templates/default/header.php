<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf8">
	<title><?php echo $data['title'].' - '.SITETITLE; //SITETITLE defined in index.php?></title>
	
	<link href="<?php echo \helpers\url::get_template_path();?>css/bootstrap/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo \helpers\url::get_template_path();?>css/style.css" rel="stylesheet">
</head>
<body>

<div id='wrapper'>
	<header class="row clearfix">
		<div class="col-md-12 column">
			<ul class="nav nav-tabs">
				<li class="active">
					<a href="#">Scores</a>
				</li>
				<!-- Si l'user n'est pas connecte, on desactive ces liens -->
				<li <?php if(!isset($_SESSION['user'])){echo 'class="disabled"';}?>>
					<a href="/ducking-games/index.php/songs">Jeu</a>
				</li>
				<li <?php if(!isset($_SESSION['user'])){echo 'class="disabled"';} ?>>
					<a href="/ducking-games/index.php/">Scores</a>
				</li>
				<li <?php if(isset($_SESSION['user'])){echo 'class="disabled"';} ?>>
					<a href="/ducking-games/index.php/login">Connexion</a>
				</li>
				<li <?php if(!isset($_SESSION['user'])){echo 'class="disabled"';} ?>>
					<a href="/ducking-games/index.php/logout"">Deconnexion</a>
				</li>
				<li class="pull-right">
					<?php
						if(!isset($_SESSION['user'])){
							echo '<span>Deconnecté</span>';
						} else {
							echo '<span>Connecté ('.$_SESSION['user']->name.')';
						}
					?>
					 <span>
					 <!--LOGOUT-->
					 </span>
				</li>
			</ul>
		</div>
	</header>
	<p class="bg-danger"><?php echo $data['error_message'];?></p>
	<p class="bg-success"><?php echo $data['good_message'];?></p>
