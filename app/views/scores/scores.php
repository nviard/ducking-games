<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<table class="table table-bordered">
			<tr>
				<th>Nom</th>
				<th>Score</th>
			</tr>
			<?php
			$logged_user_id = $_SESSION['user']->_id;
			foreach ($data['users'] as $key => $user) {
				if ($key == $logged_user_id) {
					echo '<tr class="active">';
				} else {
					echo '<tr>';
				}
				echo '<td>' . utf8_decode($user['name']) . '</td>';
				echo '<td>' . $user['score'] . '</td>';
				echo '</tr>';
			}	
			?>
		</table>
	</div>
</div>
