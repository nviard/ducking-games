<script charset="utf-8" src="<?php echo \helpers\url::get_template_path();?>js/jquery.js"></script>



<?php 
$file = $data['songs']->file; 
$id = $data['songs']->_id; 

?>

<div class="row">				
	<div class="col-xs-12">
		<audio controls="controls" preload="auto">
			<source src="<?php echo $file ?>" type="audio/mp3"/>
				Le navigateur ne semble pas compatible.
			</audio>
		</div>
	</div>

	<div id="affichage"></div>

	<div class="row">				
		<form method="post" action="index.php/songs/repondreSong" class="col-sm-6 col-sm-offset-3 col-xs-12">
			<input type="hidden" name="id" value="<?php echo $id;?>"/>
			<legend> Votre Réponse : </legend>
			<div class="form-group">
				<label for="artiste et/ou titre">Artiste et/ou Titre</label>
				<input type="text" name="reponse" class="form-control" id="reponse" />
			</div>
			
			<button type="submit" class="btn btn-primary pull-right">Valider</button>
		</form>
	</div>
</div>

<script type="text/javascript">


jQuery(document).ready(function(){
	$("#reponse").prop('disabled', false);
	

	var audio = document.getElementsByTagName("audio")[0];
	var firstTry=false;
	audio.addEventListener('play', function(){
		if(!firstTry){
	    	temp = 0;
	    	setTimeout(function(){
	    		document.getElementById("affichage").innerHTML = "Fin du compte &agrave; rebour";
				$("#reponse").prop('disabled', true);
	    	},28000);

			jQuery(function(){
				for (i=28 ;i > -1;i--){
					setTimeout("jQuery('#affichage').text('Il reste "+i+" secondes');",temp);
					temp+=1000;
				}

			});

		}
		firstTry=true;
	});
	
});

</script>
