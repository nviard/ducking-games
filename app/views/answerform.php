<div class="row">				
	<form method="post" action="" class="col-sm-12 col-xs-12">
		<legend>Votre Réponse :</legend>
		<div class="form-group">
			<label for="artiste">Artiste</label>
			<input type="text" name="artiste" class="form-control" id="artiste" />
		</div>
		<div class="form-group">
			<label for="chanson">Chanson</label>
			<input type="text" name="chanson" placeholder="" class="form-control" id="chanson" />
		</div>
		<button type="submit" class="btn btn-primary pull-right">Valider</button>
	</form>
</div>