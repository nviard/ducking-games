<div class="row">				
	<form method="post" action="" class="col-xs-12 col-sm-6 col-sm-offset-3">
		<legend>Login :</legend>
		<div class="form-group">
			<label for="email">Email :</label>
			<input type="text" name="email" class="form-control" id="email" />
		</div>
		<div class="form-group">
			<label for="chanson">Mot de passe : </label>
			<input type="password" name="password" placeholder="" class="form-control" id="password" />
		</div>
		<button type="submit" class="btn btn-primary pull-right">Valider</button>
	</form>
</div>